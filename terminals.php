<?php

require_once(dirname(__FILE__) . '../../../config/config.inc.php');
require_once(dirname(__FILE__) . '../../../init.php');

if (Tools::getValue('terminal')) {
    Db::getInstance()->update(
        'cart',
        [
            'terminal_id' => (Tools::getValue('terminal')) ? Tools::getValue('terminal') : NULL
        ],
        'id_cart = ' . (isset($context->cookie->id_cart) ? (int)$context->cookie->id_cart : 0)
    );

    $result = Tools::jsonEncode(
        array(
            'terminal_id' => Tools::getValue('terminal'),
            'id_cart' => (isset($context->cookie->id_cart) ? (int)$context->cookie->id_cart : 0)
        )
    );
    die($result);
} else {
    die('Terminal value not found');
}